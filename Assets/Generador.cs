﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generador : MonoBehaviour
{
    public GameObject balon;
    Vector3 posicion;
    //Pantalla
    float width;
    float heigth;

    Vector3 izquierda;
    Vector3 arriba;
    Vector3 abajo;
    Vector3 derecha;
    // Start is called before the first frame update
    void Start()
    {
        width = Screen.width;
        heigth = Screen.height;

        arriba = new Vector3(width / 2, heigth - 50, transform.position.z);
        abajo = new Vector3(width / 2, 60, transform.position.z);
        izquierda = new Vector3(60, heigth / 2, transform.position.z);
        derecha = new Vector3(width - 60, heigth / 2, transform.position.z);

        //posicion = new Vector3(transform.position.x*-2, transform.position.y*-2,transform.position.z);
        Instantiate(balon, arriba, GameObject.FindGameObjectWithTag("Player").transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
