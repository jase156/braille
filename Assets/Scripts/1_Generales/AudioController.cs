﻿using System;
using System.Collections;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioSource auxAudio;
    public AudioClip clipCorrecto;
    public AudioClip clipError;
    public AudioClip clipRun;
    AudioSource audio;

    private void Awake() {
        audio = GetComponent<AudioSource>();
    }
    void Start() {
        
    }
    public void playSoundCorrect(){
        audio.clip = clipCorrecto;
        audio.Play();
    }
    public void playSoundError()
    {
        audio.clip = clipError;
        audio.Play();
    }
    public void playSoundRun()
    {
        audio.clip = clipRun;
        audio.Play();
    }
    private IEnumerator pararAudio(float tiempo) {
        
        yield return new WaitForSeconds(tiempo);
        audio.Stop();
    }
    public void desactivarAudio(){
        audio.mute = true;
        auxAudio.mute = true;
    }
    public void activarAudio(){
        audio.mute = false;
        auxAudio.mute = false;

    }
}
