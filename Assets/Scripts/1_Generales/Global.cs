﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class Global : MonoBehaviour
{
    private bool tutorial;
    public bool Tutorial { get => tutorial; set => tutorial = value; }


    private int actividad;
    public int Actividad { get { return actividad; } set { actividad = value; } }

    private int lvl;
    public int Lvl { get { return lvl; } set { lvl = value; } }

    

    void Start()
    {
        Cargar();
    }

    private void Cargar()
    {
        lvl = PlayerPrefs.GetInt("lvl", 1);
        actividad = lvl;
        tutorial = false;
    }

    public void Registrar(int lvl)
    {
        PlayerPrefs.SetInt("Activo" + lvl, lvl);
    }
}
