﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipe : MonoBehaviour {
	private bool tap, swipeLeft, swipeRight, swipeUp, swipeDown;
	private bool isDraggin = false;
	private Vector2 startTouch, swipeDelta;

	public Vector2 SwipeDelta { get { return swipeDelta; } }
	public bool SwipeLeft { get { return swipeLeft; } }
	public bool SwipeRight { get { return swipeRight; } }
	public bool SwipeUp { get { return swipeUp; } }
	public bool SwipeDown { get { return swipeDown; } }
	public bool Tap { get { return tap; } }

	private void Update () {
		tap = swipeLeft = swipeRight = swipeUp = swipeDown = false;

		if (Input.GetMouseButtonDown (0)) {
			tap = true;
			startTouch = Input.mousePosition;
			isDraggin = true;
		} else if (Input.GetMouseButtonUp (0)) {
			isDraggin = false;
			Reset ();
		}

		//mobile inputs
		if (Input.touches.Length == 1) {
			if (Input.touches[0].phase == TouchPhase.Began) {
				tap = true;
				startTouch = Input.touches[0].position;
				isDraggin = true;
			} else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled) {
				isDraggin = false;
				Reset ();
			}
		} else if (Input.touches.Length == 2) {
			if (Input.touches[1].phase == TouchPhase.Began) {
				tap = true;
				startTouch = Input.touches[1].position;
				isDraggin = true;
			} else if (Input.touches[1].phase == TouchPhase.Ended || Input.touches[1].phase == TouchPhase.Canceled) {
				isDraggin = false;
				Reset ();
			}
		} else if (Input.touches.Length > 2) {
			if (Input.touches[2].phase == TouchPhase.Began) {
				tap = true;
				startTouch = Input.touches[2].position;
				isDraggin = true;
			} else if (Input.touches[2].phase == TouchPhase.Ended || Input.touches[2].phase == TouchPhase.Canceled) {
				isDraggin = false;
				Reset ();
			}
		}

		//calculate distances
		swipeDelta = Vector2.zero;
		if (isDraggin) {
			if (Input.touches.Length == 1) {
				swipeDelta = Input.touches[0].position - startTouch;
			} else if (Input.touches.Length == 2) {
				swipeDelta = Input.touches[1].position - startTouch;
			} else if (Input.touches.Length > 2) {
				swipeDelta = Input.touches[2].position - startTouch;
			} else if (Input.GetMouseButton (0)) {
				swipeDelta = (Vector2) Input.mousePosition - startTouch;
			}
		}

		//dead zone 
		if (swipeDelta.magnitude > 105) {
			float posX = swipeDelta.x;
			float posY = SwipeDelta.y;

			//direction
			if (Mathf.Abs (posX) > Mathf.Abs (posY)) {
				//Left  -  Right
				if (posX < 0) //Left
				{
					swipeLeft = true;
				} else //Right
				{
					swipeRight = true;
				}
			} else {
				//Up  -  Down
				if (posY > 0) //Up
				{
					swipeUp = true;
				} else //Down
				{
					swipeDown = true;
				}
			}

			Reset ();
		}
	}

	private void Reset () {
		startTouch = swipeDelta = Vector2.zero;
		isDraggin = false;
	}
}