﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Touch : MonoBehaviour
{
    private bool dobleTapUnDedo, tripleTapUnDedo, dobleTapDosDedos, tapLargo, tapLargoDoble;
    private int count;

    public bool DobleTapUnDedo { get { return dobleTapUnDedo; } set { dobleTapUnDedo = value; } }
    public bool DobleTapDosDedos { get { return dobleTapDosDedos; } set { dobleTapDosDedos = value; } }
    public bool TripleTapUnDedo { get { return tripleTapUnDedo; } set { tripleTapUnDedo = value; } }
    public bool TapLargo  { get { return tapLargo; } set { tapLargo = value; } }
    public bool TapLargoDoble { get { return tapLargoDoble; } set { tapLargoDoble = value; } }


    //Tiempo de dedo sobre la pantalla
    private float touchTime;
    private bool lanzar;
    private double tiempo = 0.5;

    // Start is called before the first frame update
    void Start()
    {
        lanzar = true;
    }

    // Update is called once per frame
    void Update()
    {
        //dobleTapDosDedos = dobleTapUnDedo = tripleTapUnDedo = tapLargo = false;

        if (1 < Input.touchCount)
        {
            if (Input.GetTouch(1).phase == TouchPhase.Began)
            {
                if (Input.GetTouch(1).tapCount == 2)
                {
                    dobleTapDosDedos = true;
                }
            }
            else if (Input.GetTouch(1).phase == TouchPhase.Ended || Input.GetTouch(1).phase == TouchPhase.Canceled)
            {
                lanzar = true;
            }
            else if (Time.time - touchTime >= 1 && lanzar)
            {
                lanzar = false;
                tapLargo = true;
            }
        }
        else if (0 < Input.touchCount)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                touchTime = Time.time;
                count++;
                StartCoroutine("timer");

            }
            else if (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled)
            {
                lanzar = true;
            }
            else if (Time.time - touchTime >= 1 && lanzar)
            {
                lanzar = false;
                tapLargo = true;
            }
        }
    }

    IEnumerator timer()
    {
        while (true)
        {
            if (tiempo <= 0)
            {
                if (count == 3)
                    tripleTapUnDedo = true;                    
                else if(count == 2)
                    dobleTapUnDedo = true;
                tiempo = 0.5;
                count = 0;
                StopCoroutine("timer");
            }
            else
            {
                yield return new WaitForSeconds(0.5f);
                tiempo=-0.5;
            }
        }
    }
}
