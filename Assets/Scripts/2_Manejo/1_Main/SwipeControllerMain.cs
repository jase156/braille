﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SwipeControllerMain : MonoBehaviour
{
    public Swipe swipe;
    public Global global;
    public HablarMain hablar;
    public Image places;
    public Image left;
    public Image right;
    public Sprite lvl1;
    public Sprite lvl2;
    public Sprite lvl3;
    public Sprite lvl4;
    public Sprite lvl5;
    public Sprite lvl6;
    public Sprite lvl7;
    public Sprite lvl8;
    public Sprite lvl9;
    public Sprite lvl10;
    public Sprite lvl11;
    public Sprite lvl12;

    public Image medalla;

    public Text nivel;

    public void Start()
    {
        Imagen();
        Medalla();
    }

    private void Update()
    {
        if (swipe.SwipeLeft)
        { //Swip izquierda
            global.Tutorial = false;
            if (global.Actividad - 1 >= 1)
            {
                global.Actividad--;
                Imagen();
                Medalla();
                hablar.TTSHablar("Nivel: " + global.Actividad, "Succes");
            }
        }
        if (swipe.SwipeRight)
        { //Swip derecha
            global.Tutorial = false;
            if (global.Actividad + 1 <= 12)
            {
                global.Actividad++;
                Imagen();
                Medalla();
                hablar.TTSHablar("Nivel: " + global.Actividad, "Succes");
            }
        }
        if (swipe.SwipeUp)
        { //Swip arriba
            global.Tutorial = true;
            hablar.TTSHablar("Si quieres escuchar el tutorial nuevamente da doble tap en la pantalla. Para cancelar desliza tu dedo hacia abajo.", "Succes");

        }
        if (swipe.SwipeDown)
        { //Swip abajo    
            global.Tutorial = false;
            /*if (global.nivel - 1 >= 1)
            {
                global.nivel--;
                global.subnivel = 1;
                hablar.TTSHablar("Nivel: " + global.nivel + " Actividad: " + global.subnivel, "Succes");
            }*/
        }
    }

    void OnDestroy()
    {
        hablar.TTSParar();
    
    }

    private void Imagen()
    {
        left.enabled = true;
        right.enabled = true;
        switch (global.Actividad)
        {
            case 2:
                places.sprite = lvl2;
                break;
            case 3:
                places.sprite = lvl3;
                break;
            case 4:
                places.sprite = lvl4;
                break;
            case 5:
                places.sprite = lvl5;
                break;
            case 6:
                places.sprite = lvl6;
                break;
            case 7:
                places.sprite = lvl7;
                break;
            case 8:
                places.sprite = lvl8;
                break;
            case 9:
                places.sprite = lvl9;
                break;
            case 10:
                places.sprite = lvl10;
                break;
            case 11:
                places.sprite = lvl11;
                break;
            case 12:
                right.enabled = false;
                places.sprite = lvl12;
                break;
            default:
                left.enabled = false;
                places.sprite = lvl1;
                break;
        }
    }

    private void Medalla()
    {
        nivel.text = global.Actividad.ToString();
        if (PlayerPrefs.GetInt("Activo"+global.Actividad) != 0)
        {
            medalla.enabled = true;
        }
        else
        {
            medalla.enabled = false;
        }

    }
}