﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TouchControllerMain : MonoBehaviour
{
    public Touch touch;
    public HablarMain hablar;
    public Global global;
    public Instrucciones instruccion;

    private bool banderaInstruccion;
    
    // Start is called before the first frame update
    void Start()
    {
        banderaInstruccion = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (touch.DobleTapUnDedo)
        {
            if (global.Tutorial)
            {
                hablar.TTSParar();
                touch.DobleTapUnDedo = false;
                global.Tutorial = false;
                SceneManager.LoadScene("Tutorial");
            }
            else
            {
                hablar.TTSParar();
                touch.DobleTapUnDedo = false;
                SceneManager.LoadScene("Lvl" + global.Actividad);
            }
            
        }
        if (touch.DobleTapDosDedos)
        {
            Application.Quit();
        }
        if (touch.TripleTapUnDedo)
        {            
            if (hablar.Liberar==1)
            {
                hablar.Liberar = 2;
                hablar.TTSParar();
                banderaInstruccion = false;
                Handheld.Vibrate();
            }
            else
            {
                instruccion.Instruccion("");
                banderaInstruccion = true;
                Handheld.Vibrate();
            }
            touch.TripleTapUnDedo = false;
        }        
    }    

}
