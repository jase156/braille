﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TouchControllerLvl : MonoBehaviour
{
    public Touch touch;
    public HablarMain hablar;
    public Global global;
    public Instrucciones instruccion;
    
    // Update is called once per frame
    void Update()
    {        
        if (touch.DobleTapDosDedos)
        {
            hablar.TTSParar();
            touch.DobleTapDosDedos = false;
            SceneManager.LoadScene("Principal");
        }
        if (touch.TripleTapUnDedo)
        {
            if (hablar.LiberarAux == 1)
            {
                touch.TripleTapUnDedo = false;
                return;
            }                
            if (hablar.Liberar == 1)
            {
                hablar.TTSParar();
                hablar.Liberar = 2;
                Handheld.Vibrate();
            }
            else
            { 
                instruccion.Instruccion("");
                Handheld.Vibrate();
            }
            touch.TripleTapUnDedo = false;
        }
    }
}
