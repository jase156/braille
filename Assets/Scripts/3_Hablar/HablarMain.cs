﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HablarMain : MonoBehaviour
{
    public TextToSpeech tts;

    public int liberar, liberarAux;
    public int Liberar { get { return liberar; } set { liberar = value; } }
    public int LiberarAux { get { return liberarAux; } set { liberarAux = value; } }

    // Start is called before the first frame update
    void Start()
    {
        //Inicializar TTS        
        tts = GetComponent<TextToSpeech>();
        liberar = 0;
        liberarAux = 0;
    }
    
    //Llamado metodo Speak + Retorno de función
    public void TTSHablar(string texto, string funcion)
    {
        tts.Speak(texto, funcion);        
    }

    public void TTSParar()
    {
        tts.Stop();
    }

    public void TTSMensaje(string texto)
    {
        tts.ShowMessage(texto);
    }

    //Metodos de retorno de función hablar
    public void Succes(string msg)
    {
        switch (msg)
        {
            case "100":
                Liberar = 1;
                break;
            case "200":
                Liberar = 2;
                break;
            case "300":
                break;
        }
    }

    public void SuccesAux(string msg)
    {
        switch (msg)
        {
            case "100":
                LiberarAux = 1;
                break;
            case "200":
                LiberarAux = 2;
                break;
            case "300":
                break;
        }
    }
}
