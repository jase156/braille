﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instrucciones : MonoBehaviour
{
    public HablarMain hablar;
    public Global global;    

    public int vista;
    public int linea;
    private int reto;
    public int Reto { get { return reto; }set { reto = value; } }

    // Update is called once per frame
    void Start()
    {
        Instruccion("");
    }

    public void Instruccion(string apoyo)
    {
        string general;
        switch (vista)
        {
            case 0:
                hablar.TTSHablar("Bienvenido a Escribir con Puntos. Usted se encuentra en el nivel" + global.Actividad, "Succes");
                break;
            case 1:
                general = "Nivel 1. Desliza tu dedo a la posición indicada según las instrucciones. Al fallar el teléfono vibrara.";
                string reto = "Reto 1: Acierta al blanco 10 veces seguidas en 30 segundos. Cada fallo será penalizado con 2 segundos menos.";
                hablar.TTSHablar(general + " " + reto, "Succes");
                break;
                        
            case 111:
                reto = "Reto 2: Acierta al blanco 10 veces seguidas en 20 segundos. Cada fallo será penalizado con 2 segundos menos.";
                hablar.TTSHablar(reto, "Succes");
                break;
            case 112:
                reto = "Reto 3: Acierta al blanco 10 veces seguidas en 12 segundos. Cada fallo será penalizado con 2 segundos menos.";
                hablar.TTSHablar(reto, "Succes");
                break;
            case 2:
                general = "Nivel 2. Mueve el personaje desde el centro de la pantalla a las posiciones indicadas. Cuando estés sobre el personaje el teléfono vibrara";
                hablar.TTSHablar(general, "Succes");
                break;
            case 3:
                general = "Nivel 3. Reconoce los 6 puntos del signo generador. Memoriza y pulsa botón que representa cada punto. ";
                string punto1 = "El punto 1 se encuentra arriba a la izquierda.";
                string punto2 = "El punto 2 se encuentra en la mitad a la izquierda.";
                string punto3 = "El punto 3 se encuentra abajo a la izquierda.";
                string punto4 = "El punto 4 se encuentra arriba a la derecha.";
                string punto5 = "El punto 5 se encuentra en la mitad a la derecha.";
                string punto6 = "El punto 6 se encuentra abajo a la derecha.";
                int aux = 0;
                if (apoyo == "")
                    aux = Reto + 1;                    
                switch (Reto-aux)
                {
                    case 0:
                        hablar.TTSHablar(punto1, "SuccesAux");
                        break;
                    case 1:
                        hablar.TTSHablar(punto2, "SuccesAux");
                        break;
                    case 2:
                        hablar.TTSHablar(punto3, "SuccesAux");
                        break;
                    case 3:
                        hablar.TTSHablar(punto4, "SuccesAux");
                        break;
                    case 4:
                        hablar.TTSHablar(punto5, "SuccesAux");
                        break;
                    case 5:
                        hablar.TTSHablar(punto6, "SuccesAux");
                        break;
                    default:
                        hablar.TTSHablar(general, "Succes");
                        break;
                }
                break;
            case 4:
                general = "Bien echo, repitamos nuevamente. Oprime el botón que representa cada punto. ";
                punto1 = "El punto 1 se encuentra arriba a la izquierda.";
                punto2 = "El punto 2 se encuentra en la mitad a la izquierda.";
                punto3 = "El punto 3 se encuentra abajo a la izquierda.";
                punto4 = "El punto 4 se encuentra arriba a la derecha.";
                punto5 = "El punto 5 se encuentra en la mitad a la derecha.";
                punto6 = "El punto 6 se encuentra abajo a la derecha.";
                aux = 0;
                if (apoyo == "")
                    aux = Reto + 1;
                switch (Reto - aux)
                {
                    case 0:
                        hablar.TTSHablar(punto1, "SuccesAux");
                        break;
                    case 1:
                        hablar.TTSHablar(punto2, "SuccesAux");
                        break;
                    case 2:
                        hablar.TTSHablar(punto3, "SuccesAux");
                        break;
                    case 3:
                        hablar.TTSHablar(punto4, "SuccesAux");
                        break;
                    case 4:
                        hablar.TTSHablar(punto5, "SuccesAux");
                        break;
                    case 5:
                        hablar.TTSHablar(punto6, "SuccesAux");
                        break;
                    default:
                        hablar.TTSHablar(general, "Succes");
                        break;
                }
                break;
            case 5:
                general = "Nivel 4. Ahora hazlo tu solo. Selecciona en orden todos los puntos del signo generador.";
                string reto1 = "Bien echo, repitamos nuevamente";
                switch (Reto)
                {
                    case 0:
                        hablar.TTSHablar(general, "Succes");
                        break;
                    case 1:
                        hablar.TTSHablar(reto1, "Succes");
                        break;
                }
                break;
            case 6:
                general = "Nivel 5. Memoriza los puntos y acierta correctamente";
                switch (Reto)
                {
                    case 0:
                        hablar.TTSHablar(general, "Succes");
                        break;
                }
                break;
            case 7:
                general = "Nivel 6. Primera serie. La primera serie del sistema braille cuenta con 10 letras. Memoriza y pulsa los puntos de cada letra.";
                reto1 = "Bien echo, repitamos nuevamente";
                switch (Reto)
                {
                    case 0:
                        hablar.TTSHablar(general, "Succes");
                        break;
                    case 1:
                        hablar.TTSHablar(reto1, "Succes");
                        break;
                }
                break;
            case 8:
                general = "Nivel 7. Seleccione los puntos que correspondan a cada letra de la primera serie. ";
                switch (Reto)
                {
                    case 0:
                        hablar.TTSHablar(general, "Succes");
                        break;
                }
                break;
            case 9:
                general = "Nivel 8. Segunda serie. La segunda serie del sistema braille cuenta con 10 letras. Memoriza y pulsa los puntos de cada letra.";
                reto1 = "Bien echo, repitamos nuevamente";
                switch (Reto)
                {
                    case 0:
                        hablar.TTSHablar(general, "Succes");
                        break;
                    case 1:
                        hablar.TTSHablar(reto1, "Succes");
                        break;
                }
                break;
            case 10:
                general = "Nivel 9. Seleccione los puntos que correspondan a cada letra de la segunda serie. ";
                switch (Reto)
                {
                    case 0:
                        hablar.TTSHablar(general, "Succes");
                        break;
                }
                break;
            case 11:
                general = "Nivel 10. Tercera serie. La tercera serie del sistema braille cuenta con 5 letras. Memoriza y pulsa los puntos de cada letra.";
                reto1 = "Bien echo, repitamos nuevamente";
                switch (Reto)
                {
                    case 0:
                        hablar.TTSHablar(general, "Succes");
                        break;
                    case 1:
                        hablar.TTSHablar(reto1, "Succes");
                        break;
                }
                break;
            case 12:
                general = "Nivel 11. Seleccione los puntos que correspondan a cada letra de la tercera serie. ";
                reto1 = "Felicidades!! terminaste con éxito todos los niveles de escribir con puntos. El siguiente nivel, te permitirá repasar lo aprendido";
                switch (Reto)
                {
                    case 0:
                        hablar.TTSHablar(general, "Succes");
                        break;
                    case 1:
                        hablar.TTSHablar(reto1, "Succes");
                        break;
                }
                break;
            case 13:
                general = "Nivel 12. Seleccione los puntos que correspondan a cada letra";
                switch (Reto)
                {
                    case 0:
                        hablar.TTSHablar(general, "Succes");
                        break;
                }
                break;
        }
    }

    void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            hablar.TTSParar();
        }
        else
        {
            Instruccion("");
        }
    }
}
