﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tutorial : MonoBehaviour
{

    public HablarMain hablar;
    public Global global;
    public Text texto;
    public AudioController audio;

    private int count = 0;
    private int aud = 0;
    private string[] instrucciones;

    // Start is called before the first frame update
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        instrucciones = new string[]{
            "Bienvenido a escribir con puntos, una herramienta que permite aprender los conceptos básicos del sistema Braille.",
            "La aplicación está diseñada para ser usada a base de gestos.",
            "Si te encuentras en el menú principal y quieres avanzar de nivel desliza tú dedo de izquierda a derecha; si quieres retroceder de nivel desliza tú dedo de derecha a izquierda.",
            "Para ingresar a un nivel desde el menú principal da doble tap con un dedo en la pantalla; si quieres regresar al menú principal desde cualquier nivel, da doble tap con dos dedos en la pantalla.",
            "Si quieres repetir una instrucción da triple tap con un dedo en la pantalla.",
            "Cada nivel te dará las instrucciones necesarias para poder completarlo con éxito; siempre  que falles al cumplir una instrucción escucharás el siguiente sonido.",
            "AUD00",
            "Cuando cumplas la instrucción de forma correcta, escucharás el siguiente sonido.",
            "AUD00",
            "Para escuchar el tutorial nuevamente desliza tu dedo hacia arriba en el menú principal.",
            "Esperamos que te diviertas y aprendas mucho junto a Escribir con puntos.",

        };

        StartCoroutine("rutina");

        
        hablar.TTSHablar(instrucciones[count], "Succes");
        count++;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    

    void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            StopAllCoroutines();
            hablar.TTSParar();
            count--;
            
        }
        else
        {
            StartCoroutine("rutina");
            hablar.TTSHablar(instrucciones[count], "Succes");
            count++;
        }
    }

    void OnDestroy()
    {
        hablar.TTSParar();

    }

    IEnumerator rutina()
    {        
        while (true)
        {
            if (hablar.Liberar == 2)
            {
                
                if (instrucciones.Length > count)
                {
                    if (instrucciones[count].Equals("AUD00"))
                    {
                        switch (aud)
                        {
                            case 0:
                                audio.playSoundError();
                                break;
                            case 1:
                                audio.playSoundCorrect();
                                break;
                        }
                        aud++;
                    }
                    else
                    {
                        hablar.TTSHablar(instrucciones[count], "Succes");
                        texto.text = instrucciones[count];
                    }                    
                }
                else
                {
                    StopAllCoroutines();
                    PlayerPrefs.SetInt("tutorial", 1);
                    SceneManager.LoadScene("Principal");
                }
                    
                count++;
            }
            yield return new WaitForSeconds(0.5f);
        }
    }
}
