﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Arrastrar : MonoBehaviour, IDragHandler
{
    public GameObject ball;
    public void OnDrag(PointerEventData eventData)
    {
        //transform.position = eventData.position;
        transform.position = Camera.main.ScreenToWorldPoint(eventData.position);
    }
}
