﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BallMove : MonoBehaviour
{

    public HablarMain hablar;
    public Swipe swipe;
    public Global global;
    public Instrucciones instrucciones;
    public Text scores;
    public Text regresiva;

    //Jugabilidad
    private float padding = 50;
    public float BallSpeed;
    public int score = 0;
    public int golpe = 0;
    private bool fin;

    //Timer
    public int tiempoMax;
    public int tiempo;

    //Posición del balon
    private float izquierda;
    private float derecha;
    private float centroV;
    private float centroH;
    private float arriba;
    private float abajo;    

    void Start()
    {
        derecha = transform.position.x*2;
        arriba = transform.position.y*2;
        izquierda = 0;
        abajo = 0;
        centroH = transform.position.x;
        centroV = transform.position.y;

        fin = false;

        StartCoroutine("rutina");
    }
    
    void Update()
    {
        if(hablar.Liberar == 0)
        {
            if (swipe.SwipeLeft)
            {
                transform.position = new Vector3(izquierda + padding, centroV, 0);
            }
            if (swipe.SwipeRight)
            {
                transform.position = new Vector3(derecha - padding, centroV, 0);
            }
            if (swipe.SwipeUp)
            {
                transform.position = new Vector3(centroH, arriba - padding, 0);
            }
            if (swipe.SwipeDown)
            {
                transform.position = new Vector3(centroH, abajo + padding, 0);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Bottom")
        {
            if(golpe == 2)
            {
                avance();
            }
            else
            {
                Handheld.Vibrate();
                tiempoMax = tiempoMax - 2;
            }
        }
        if (collision.gameObject.name == "Top")
        {
            if (golpe == 1)
            {
                avance();
            }
            else
            {
                Handheld.Vibrate();
                tiempoMax = tiempoMax - 2;
            }
        }
        if (collision.gameObject.name == "Left")
        {
            if (golpe == 3)
            {
                avance();
            }
            else
            {
                Handheld.Vibrate();
                tiempoMax = tiempoMax - 2;
            }
        }
        if (collision.gameObject.name == "Right")
        {
            if (golpe == 4)
            {
                avance();
            }
            else
            {
                Handheld.Vibrate();
                tiempoMax = tiempoMax - 2;
            }
        }
        scores.text = "Aciertos: "+score.ToString();
    }

    public void avance()
    {
        score++;
        if (score < 10)
        {
            accion();
        }
        else
        {            
            StopCoroutine("timer");
            score = 0;
            string apoyo ="" ;
            tiempo = tiempo - tiempoMax;
            apoyo = "Bien echo, lo hiciste en " + tiempo + " segundos.";
            switch (instrucciones.vista)
            {
                case 1:
                    fin = true;
                    hablar.TTSHablar(apoyo, "Succes");
                    break;
                case 111:
                    fin = true;
                    hablar.TTSHablar(apoyo, "Succes");
                    break;
                case 112:
                    fin = true;
                    hablar.TTSHablar(apoyo + "Avanzas de nivel", "Succes");
                    global.Actividad = global.Lvl = 2;
                    PlayerPrefs.SetInt("sublvl", global.Lvl);
                    global.Registrar(1);
                    break;
            }
        }
    }

    public void accion()
    {                
        System.Random rnd = new System.Random();
        int opcion = rnd.Next(1, 5);
        while (opcion == golpe)
        {
            opcion = rnd.Next(1, 5);
        }
        switch (opcion)
        {
            case 1:
                golpe = 1;
                hablar.TTSHablar("Arriba", "");
                break;
            case 2:
                golpe = 2;
                hablar.TTSHablar("Abajo", "");
                break;
            case 3:
                golpe = 3;
                hablar.TTSHablar("Izquierda", "");
                break;
            case 4:
                golpe = 4;
                hablar.TTSHablar("Derecha", "");
                break;
        }        
    }
    
    IEnumerator rutina()
    {
        while (true)
        {
            if (hablar.Liberar == 1)
            {
                StopCoroutine("timer");
                transform.position = new Vector3(centroH, centroV, 0);
            }
            else if (hablar.Liberar == 2)
            {
                if (fin)
                {
                    StopCoroutine("timer");
                    hablar.TTSParar();
                    StopAllCoroutines();
                    switch (instrucciones.vista)
                    {
                        case 1:
                            SceneManager.LoadScene("Lvl1.1");
                            break;
                        case 111:
                            SceneManager.LoadScene("Lvl1.2");
                            break;
                        case 112:
                            SceneManager.LoadScene("Lvl2");
                            break;
                    }
                }                    
                else {
                    hablar.TTSParar();
                    transform.position = new Vector3(centroH, centroV, 0);
                    while (hablar.Liberar != 0)
                    {
                        hablar.Liberar = 0;
                    }                    
                    StartCoroutine("timer");
                    accion();
                }
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

    IEnumerator timer()
    {
        while (true)
        {
            if (tiempoMax <= 0)
            {
                while (hablar.Liberar != 0)
                {
                    hablar.Liberar = 0;
                }
                transform.position = new Vector3(centroH, centroV, 0);
                instrucciones.Instruccion("");
                tiempoMax = tiempo;
                score = 0;
                break;
            }
            else
            {
                yield return new WaitForSeconds(1f);
                tiempoMax--;
                regresiva.text = tiempoMax.ToString();
            }
        }
    }
}
