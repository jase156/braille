﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SerieAleatorio : MonoBehaviour
{

    public Button punto1, punto2, punto3, punto4, punto5, punto6;
    public Instrucciones instrucciones;
    public AudioController audio;
    public HablarMain hablar;
    public Global global;

    public Image ball1, ball2, ball3, ball4, ball5, ball6;
    public Text balon1, balon2, balon3, balon4, balon5, balon6;

    Hashtable serie;
    string[] letras;
    private int[] puntos;
    private int count;
    private int letra;
    private int golpe;

    // Start is called before the first frame update
    void Start()
    {
        punto1.onClick.AddListener(delegate { click(1); });
        punto2.onClick.AddListener(delegate { click(2); });
        punto3.onClick.AddListener(delegate { click(3); });
        punto4.onClick.AddListener(delegate { click(4); });
        punto5.onClick.AddListener(delegate { click(5); });
        punto6.onClick.AddListener(delegate { click(6); });

        serie = new Hashtable();
        serie["a"] = new int[] { 1 };
        serie["b"] = new int[] { 1, 2 };
        serie["c"] = new int[] { 1, 4 };
        serie["d"] = new int[] { 1, 4, 5 };
        serie["e"] = new int[] { 1, 5 };
        serie["f"] = new int[] { 1, 2, 4 };
        serie["g"] = new int[] { 1, 2, 4, 5 };
        serie["h"] = new int[] { 1, 2, 5 };
        serie["i"] = new int[] { 2, 4 };
        serie["j"] = new int[] { 2, 4, 5 };
        serie["k"] = new int[] { 1, 3 };
        serie["l"] = new int[] { 1, 2, 3 };
        serie["m"] = new int[] { 1, 3, 4 };
        serie["n"] = new int[] { 1, 3, 4, 5 };
        serie["o"] = new int[] { 1, 3, 5 };
        serie["p"] = new int[] { 1, 2, 3, 4 };
        serie["q"] = new int[] { 1, 2, 3, 4, 5 };
        serie["r"] = new int[] { 1, 2, 3, 5 };
        serie["s"] = new int[] { 2, 3, 4 };
        serie["t"] = new int[] { 2, 3, 4, 5 };
        serie["u"] = new int[] { 1, 3, 6 };
        serie["v"] = new int[] { 1, 2, 3, 6 };
        serie["x"] = new int[] { 1, 3, 4, 6 };
        serie["ye"] = new int[] { 1, 3, 4, 5, 6 };
        serie["z"] = new int[] { 1, 3, 5, 6 };

        letras = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "ye", "z" };

        ocultar();

    }

    private void FixedUpdate()
    {
        if (hablar.Liberar == 1)
        {
            //hablar.TTSMensaje("Entra a: 1");
        }
        else if (hablar.Liberar == 2)
        {
            //hablar.TTSMensaje("Entra a: 2");
            hablar.Liberar = 0;
            comienza(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void comienza(bool inicia)
    {
        if (inicia)
        {
            System.Random rnd = new System.Random();
            count = rnd.Next(0, 25);
            puntos = (int[])serie[letras[count]];
        }            
        ocultar();
        string direcciones = "Letra " + letras[count];
        hablar.TTSHablar(direcciones, "SuccesAux");
        golpe = puntos[letra];
    }

    void click(int boton)
    {
        if (hablar.Liberar == 1 || hablar.LiberarAux == 1)
            return;
        switch (boton)
        {
            case 1:
                ball1.enabled = balon1.enabled = true;
                validar(boton);
                break;
            case 2:
                ball2.enabled = balon2.enabled = true;
                validar(boton);
                break;
            case 3:
                ball3.enabled = balon3.enabled = true;
                validar(boton);
                break;
            case 4:
                ball4.enabled = balon4.enabled = true;
                validar(boton);
                break;
            case 5:
                ball5.enabled = balon5.enabled = true;
                validar(boton);
                break;
            case 6:
                ball6.enabled = balon6.enabled = true;
                validar(boton);
                break;
        }
    }

    void validar(int boton)
    {
        hablar.TTSMensaje("Entro en validar con boton " + boton);
        if (boton == golpe)
        {
            audio.playSoundCorrect();
            letra++;
            if (letra < puntos.Length)
            {
                golpe = puntos[letra];
            }
            else
            {
                letra = 0;
                comienza(true);
            }
        }
        else
        {
            audio.playSoundError();
            letra = 0;
            comienza(false);
        }
    }

    private void ocultar()
    {
        ball1.enabled = balon1.enabled = false;
        ball2.enabled = balon2.enabled = false;
        ball3.enabled = balon3.enabled = false;
        ball4.enabled = balon4.enabled = false;
        ball5.enabled = balon5.enabled = false;
        ball6.enabled = balon6.enabled = false;
    }
}
