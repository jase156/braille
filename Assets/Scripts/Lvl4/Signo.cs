﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Signo : MonoBehaviour
{
    public Button punto1, punto2, punto3, punto4, punto5, punto6;
    public Instrucciones instrucciones;
    public AudioController audio;
    public HablarMain hablar;
    public Global global;

    public Image ball1, ball2, ball3, ball4, ball5, ball6;
    public Text balon1, balon2, balon3, balon4, balon5, balon6;

    private int count = 1;

    // Start is called before the first frame update
    void Start()
    {
        punto1.onClick.AddListener(delegate { click(1); });
        punto2.onClick.AddListener(delegate { click(2); });
        punto3.onClick.AddListener(delegate { click(3); });
        punto4.onClick.AddListener(delegate { click(4); });
        punto5.onClick.AddListener(delegate { click(5); });
        punto6.onClick.AddListener(delegate { click(6); });

        ocultar();
    }

    private void FixedUpdate()
    {
        if (hablar.Liberar == 1)
        {
            //hablar.TTSMensaje("Entra a: 1");
        }
        else if (hablar.Liberar == 2)
        {
            //hablar.TTSMensaje("Entra a: 2");
            hablar.Liberar = 0;
        }
    }

    void click(int boton)
    {
        if (hablar.Liberar == 1 || hablar.LiberarAux == 1)
            return;
        switch (boton)
        {
            case 1:
                ball1.enabled = balon1.enabled = true;
                validar(boton);
                break;
            case 2:
                ball2.enabled = balon2.enabled = true;
                validar(boton);
                break;
            case 3:
                ball3.enabled = balon3.enabled = true;
                validar(boton);
                break;
            case 4:
                ball4.enabled = balon4.enabled = true;
                validar(boton);
                break;
            case 5:
                ball5.enabled = balon5.enabled = true;
                validar(boton);
                break;
            case 6:
                ball6.enabled = balon6.enabled = true;
                validar(boton);
                break;
        }
    }

    private void validar(int boton)
    {
        if (boton == count)
        {
            audio.playSoundCorrect();
            count++;
            if (boton == 6)
            {
                if (instrucciones.Reto == 0)
                {
                    ocultar();
                    count = 1;
                    instrucciones.Reto++;
                    instrucciones.Instruccion("");
                }
                else
                {
                    global.Lvl = global.Actividad = 5;
                    PlayerPrefs.SetInt("lvl", global.Lvl);
                    global.Registrar(4);
                    SceneManager.LoadScene("Lvl5");
                }
                    
            }            
        }
        else
        {
            audio.playSoundError();
        }
    }

    void ocultar()
    {
        ball1.enabled = balon1.enabled = false;
        ball2.enabled = balon2.enabled = false;
        ball3.enabled = balon3.enabled = false;
        ball4.enabled = balon4.enabled = false;
        ball5.enabled = balon5.enabled = false;
        ball6.enabled = balon6.enabled = false;
    }
}
