﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SignoAleatorio : MonoBehaviour
{
    public Button punto1, punto2, punto3, punto4, punto5, punto6;
    public Instrucciones instrucciones;
    public AudioController audio;
    public HablarMain hablar;
    public Global global;

    public Image ball1, ball2, ball3, ball4, ball5, ball6;
    public Text balon1, balon2, balon3, balon4, balon5, balon6;

    //lista
    private int[] puntos;
    private int movimiento;
    private int golpe;
    private int count;
    private int score;

    private System.Random rnd;

    // Start is called before the first frame update
    void Start()
    {
        punto1.onClick.AddListener(delegate { click(1); });
        punto2.onClick.AddListener(delegate { click(2); });
        punto3.onClick.AddListener(delegate { click(3); });
        punto4.onClick.AddListener(delegate { click(4); });
        punto5.onClick.AddListener(delegate { click(5); });
        punto6.onClick.AddListener(delegate { click(6); });

        ocultar();
        rnd = new System.Random();
        movimiento = rnd.Next(1, 7);
    }

    private void FixedUpdate()
    {
        if (hablar.Liberar == 1)
        {
        }
        else if (hablar.Liberar == 2)
        {
            hablar.Liberar = 0;
            comienza(true);
        }
    }
    

    private void genera()
    {
        
        puntos = new int[movimiento];
        int numero;
        for (int i = 0; i < movimiento; i++)
        {
            try
            {
                do
                {
                    numero = rnd.Next(1, 7);                    
                } while (existe(numero));
                puntos[i] = numero;
            }
            catch (Exception e)
            {

            }
        }
        ordenar();
    }

    private bool existe(int punto)
    {
        return Array.Exists(puntos,element => element == punto);
    }

    private void ordenar()
    {
        int aux;
        for(int i = 0; i < puntos.Length; i++)
        {
            for (int j = i + 1; j < puntos.Length; j++)
            {
                if (puntos[j] < puntos[i])
                {
                    aux = puntos[i];
                    puntos[i] = puntos[j];
                    puntos[j] = aux;
                }
            }
        }
    }

    private void comienza(bool inicia)
    {
        if (inicia)
            genera();
        ocultar();
        string direcciones = "Puntos";
        for (int i = 0; i < puntos.Length; i++)
        {
            switch (puntos[i])
            {
                case 1:
                    direcciones = direcciones + " 1";
                    break;
                case 2:
                    direcciones = direcciones + " 2";
                    break;
                case 3:
                    direcciones = direcciones + " 3";
                    break;
                case 4:
                    direcciones = direcciones + " 4";
                    break;
                case 5:
                    direcciones = direcciones + " 5";
                    break;
                case 6:
                    direcciones = direcciones + " 6";
                    break;
            }
        }
        hablar.TTSHablar(direcciones, "SuccesAux");
        golpe = puntos[count];
    }

    void click(int boton)
    {
        if (hablar.Liberar == 1 || hablar.LiberarAux == 1)
            return;
        switch (boton)
        {
            case 1:
                ball1.enabled = balon1.enabled = true;
                validar(boton);
                break;
            case 2:
                ball2.enabled = balon2.enabled = true;
                validar(boton);
                break;
            case 3:
                ball3.enabled = balon3.enabled = true;
                validar(boton);
                break;
            case 4:
                ball4.enabled = balon4.enabled = true;
                validar(boton);
                break;
            case 5:
                ball5.enabled = balon5.enabled = true;
                validar(boton);
                break;
            case 6:
                ball6.enabled = balon6.enabled = true;
                validar(boton);
                break;
        }
    }

    private void validar(int boton)
    {
        if (boton == golpe)
        {
            audio.playSoundCorrect();            
            count++;
            if (count < movimiento)
            {
                golpe = puntos[count];
            }
            else
            {
                movimiento = rnd.Next(1, 7);
                count = 0;
                score++;
                if (score < 7)
                {
                    comienza(true);
                }else
                {
                    global.Lvl = global.Actividad = 6;
                    PlayerPrefs.SetInt("lvl", global.Lvl);
                    global.Registrar(5);
                    SceneManager.LoadScene("Lvl6");
                }
            }
        }
        else
        {
            audio.playSoundError();
            count = 0;
            comienza(false);
        }
    }

    void ocultar()
    {
        ball1.enabled = balon1.enabled = false;
        ball2.enabled = balon2.enabled = false;
        ball3.enabled = balon3.enabled = false;
        ball4.enabled = balon4.enabled = false;
        ball5.enabled = balon5.enabled = false;
        ball6.enabled = balon6.enabled = false;
    }
}
