﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SegundaSerie : MonoBehaviour
{
    public Button punto1, punto2, punto3, punto4, punto5, punto6;
    public Instrucciones instrucciones;
    public AudioController audio;
    public HablarMain hablar;
    public Global global;

    public Image ball1, ball2, ball3, ball4, ball5, ball6;
    public Text balon1, balon2, balon3, balon4, balon5, balon6;

    Hashtable serie;
    string[] letras;
    private int[] puntos;
    private int count;
    private int letra;
    private int golpe;
    public bool fin;

    // Start is called before the first frame update
    void Start()
    {
        punto1.onClick.AddListener(delegate { click(1); });
        punto2.onClick.AddListener(delegate { click(2); });
        punto3.onClick.AddListener(delegate { click(3); });
        punto4.onClick.AddListener(delegate { click(4); });
        punto5.onClick.AddListener(delegate { click(5); });
        punto6.onClick.AddListener(delegate { click(6); });

        serie = new Hashtable();
        serie["k"] = new int[] { 1, 3 };
        serie["l"] = new int[] { 1, 2, 3 };
        serie["m"] = new int[] { 1, 3, 4 };
        serie["n"] = new int[] { 1, 3, 4, 5 };
        serie["o"] = new int[] { 1, 3, 5 };
        serie["p"] = new int[] { 1, 2, 3, 4 };
        serie["q"] = new int[] { 1, 2, 3, 4, 5 };
        serie["r"] = new int[] { 1, 2, 3, 5 };
        serie["s"] = new int[] { 2, 3, 4 };
        serie["t"] = new int[] { 2, 3, 4, 5 };

        letras = new string[] { "k", "l", "m", "n", "o", "p", "q", "r", "s", "t" };
        
        ocultar();
    }

    private void FixedUpdate()
    {
        if (hablar.Liberar == 1)
        {
            //hablar.TTSMensaje("Entra a: 1");
        }
        else if (hablar.Liberar == 2)
        {
            //hablar.TTSMensaje("Entra a: 2");
            hablar.Liberar = 0;
            comienza(true);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void click(int boton)
    {
        if (hablar.Liberar == 1 || hablar.LiberarAux == 1)
            return;
        switch (boton)
        {
            case 1:
                ball1.enabled = balon1.enabled = true;
                validar(boton);
                break;
            case 2:
                ball2.enabled = balon2.enabled = true;
                validar(boton);
                break;
            case 3:
                ball3.enabled = balon3.enabled = true;
                validar(boton);
                break;
            case 4:
                ball4.enabled = balon4.enabled = true;
                validar(boton);
                break;
            case 5:
                ball5.enabled = balon5.enabled = true;
                validar(boton);
                break;
            case 6:
                ball6.enabled = balon6.enabled = true;
                validar(boton);
                break;
        }
    }

    private void comienza(bool inicia)
    {
        if(inicia)        
            puntos = (int[])serie[letras[count]];
        ocultar();
        string direcciones = "La letra " + letras[count]+ " es representada por los puntos ";
        for (int i = 0; i < puntos.Length; i++)
        {
            direcciones = direcciones + puntos[i] + " ";
        }
        hablar.TTSHablar(direcciones, "SuccesAux");
        golpe = puntos[letra];
    }

    private void validar(int boton)
    {
        if (boton == golpe)
        {
            audio.playSoundCorrect();
            letra++;
            if (letra < puntos.Length)
            {
                golpe = puntos[letra];
            }
            else
            {
                count++;
                letra = 0;
                if (count < letras.Length)
                {
                    comienza(true);
                }
                else
                {
                    if (fin)
                    {
                        global.Lvl = global.Actividad = 9;
                        PlayerPrefs.SetInt("lvl", global.Lvl);
                        global.Registrar(8);
                        SceneManager.LoadScene("Lvl9");
                    }
                    else
                    {
                        fin = true;
                        count = 0;
                        instrucciones.Reto++;
                        instrucciones.Instruccion("");
                    }
                    
                }
            }
        }
        else
        {
            audio.playSoundError();
            letra = 0;
            comienza(false);
        }
    }

    private void ocultar()
    {
        ball1.enabled = balon1.enabled = false;
        ball2.enabled = balon2.enabled = false;
        ball3.enabled = balon3.enabled = false;
        ball4.enabled = balon4.enabled = false;
        ball5.enabled = balon5.enabled = false;
        ball6.enabled = balon6.enabled = false;
    }
}
