﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MovimientoBalon : MonoBehaviour //IDragHandler
{

    //Pantalla
    float width;
    float heigth;

    Vector3 centro;

    //Drag
    private Vector3 offset;
    private Vector3 newPosition;
    private Vector3 prePosition;    
        

    // Start is called before the first frame update
    void Start()
    {
        width = Screen.width;
        heigth = Screen.height;

        centro = new Vector3(width/2, heigth/2, transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        prePosition = newPosition;
    }

    void OnMouseDown()
    {
        offset = gameObject.transform.position -
            Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));

    }

    void OnMouseDrag()
    {

        newPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);
        transform.position = Camera.main.ScreenToWorldPoint(newPosition) + offset;       

    }



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Abajo")
        {
            
            Instantiate(gameObject, centro, transform.rotation);
            Handheld.Vibrate();
        }
        if (collision.gameObject.name == "Arriba")
        {
            Instantiate(gameObject, centro, transform.rotation);
            Handheld.Vibrate();
        }
        if (collision.gameObject.name == "Izquierda")
        {
            Instantiate(gameObject, centro, transform.rotation);
            Handheld.Vibrate();
        }
        if (collision.gameObject.name == "Derecha")
        {
            Instantiate(gameObject, centro, transform.rotation);
            Handheld.Vibrate();
        }
    }
}
