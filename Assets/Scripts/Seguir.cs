﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seguir : MonoBehaviour
{
    public Transform Player;  //Asignar el personaje al que seguirán
    float MoveSpeed = 4; //Establecer velocidad de persecución
    float MaxDist = 200; //Establecer distancia máxima a la que lo seguirá
    float MinDist = 100;//Establecer distancia mínima a la que lo seguirá
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        var EnemyPos = transform.position;
        var PlayerPos = Player.position;
        var distancia = Vector3.Distance(EnemyPos, PlayerPos);        
        var targetPos = new Vector3(Player.position.x,
                                        Player.position.y-90,
                                        Player.position.z);
        transform.position = Camera.main.ScreenToWorldPoint(targetPos);

    }
}
