﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class SnakeMoved : MonoBehaviour
{
    //Generales
    public HablarMain hablar;
    public AudioController audio;
    public Global global;


    float posicionx;
    float posiciony;

    float distanciaMov = 8;

    private Animator animation;

    //Drag
    private Vector3 offset;
    private Vector3 newPosition;
    private Vector3 prePosition;
    private float centroH;
    private float centroV;

    //lista
    private int[] puntos;
    private int movimiento;
    private int golpe;
    private int count;
    private int score;
    private bool fin;
    

    // Start is called before the first frame update
    void Start()
    {
        animation = GetComponent<Animator>();
        centroH = transform.position.x;
        centroV = transform.position.y;

        movimiento = 2;

        Camera.main.orthographicSize = Screen.height / 2;

    }

    private void FixedUpdate()
    {
        if (hablar.Liberar == 1)
        {
            //hablar.TTSMensaje("Entra a: 1");
        }
        else if (hablar.Liberar == 2)
        {
            //hablar.TTSMensaje("Entra a: 2");
            hablar.Liberar = 0;
            if (fin)
                SceneManager.LoadScene("Lvl" + global.Lvl);
            else             
                comienza(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        prePosition = newPosition;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (hablar.LiberarAux == 1 || hablar.Liberar == 1)
            return;
        if (hablar.Liberar != 1)
        {
            if (collision.gameObject.name == "Arriba")
            {
                if (golpe == 1)
                {
                    avance();
                }
                else
                {
                    audio.playSoundError();
                    count = 0;
                    comienza(false);
                }
            }
            if (collision.gameObject.name == "Abajo")
            {
                if (golpe == 2)
                {
                    avance();
                }
                else
                {
                    audio.playSoundError();
                    count = 0;
                    comienza(false);
                }
            }
            if (collision.gameObject.name == "Izquierda")
            {
                if (golpe == 3)
                {
                    avance();
                }
                else
                {
                    audio.playSoundError();
                    count = 0;
                    comienza(false);
                }
            }
            if (collision.gameObject.name == "Derecha")
            {
                if (golpe == 4)
                {
                    avance();
                }
                else
                {
                    audio.playSoundError();
                    count = 0;
                    comienza(false);
                }
            }
        }        
    }

    private void comienza(bool inicia)
    {
        if (inicia)
            genera();
        string direcciones = "";
        for (int i = 0; i < puntos.Length; i++)
        {
            switch (puntos[i])
            {
                case 1:
                    direcciones = direcciones + " arriba";
                    break;
                case 2:
                    direcciones = direcciones + " abajo";
                    break;
                case 3:
                    direcciones = direcciones + " izquierda";
                    break;
                case 4:
                    direcciones = direcciones + " derecha";
                    break;
            }
        }
        hablar.TTSHablar(direcciones, "SuccesAux");
        golpe = puntos[count];
    }

    private void genera()
    {
        System.Random rnd = new System.Random();
        puntos = new int[movimiento];
        for (int i = 0; i < movimiento; i++)
        {
            puntos[i] = rnd.Next(1, 5);
            try
            {
                while (puntos[i] == puntos[i - 1])
                {
                    puntos[i] = rnd.Next(1, 5);
                }
            }
            catch (Exception e)
            {

            }
        }
    }

    private void avance()
    {        
        audio.playSoundCorrect();
        count++;
        if (count < movimiento)
        {
            golpe = puntos[count];
        }
        else
        {
            movimiento++;
            count = 0;
            score++;
            if (score < 5)
            {
                comienza(true);
            }
            else
            {
                fin = true;
                global.Lvl = global.Actividad = 3;
                PlayerPrefs.SetInt("lvl", global.Lvl);
                global.Registrar(2);
                hablar.TTSHablar("Bien echo avanzas de nivel", "Succes");
            }
        }
    }   

    private void OnMouseDown()
    {
        offset = gameObject.transform.position -
            Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 4f));
        Handheld.Vibrate();
    }
        
    private void OnMouseUp()
    {
        animation.SetTrigger("Parar");
        audio.playSoundRun();
        transform.position = new Vector3(centroH, centroV, 0);
    }    

    private void OnMouseDrag()
    {

        newPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 4f);
        transform.position = Camera.main.ScreenToWorldPoint(newPosition) + offset;

        posicionx = newPosition.x - prePosition.x;
        posiciony = newPosition.y - prePosition.y;

        
        if (Math.Abs(posicionx) > Math.Abs(posiciony))
        {
            if (posicionx > 0 && Math.Abs(posicionx)>= distanciaMov)
            {
                animation.SetTrigger("Derecha");
            }
            else if(Math.Abs(posicionx) >= distanciaMov)
            {
                animation.SetTrigger("Izquierda");
            }
        }
        else
        {
            if (posiciony > 0 && Math.Abs(posiciony) >= distanciaMov)
            {
                animation.SetTrigger("Arriba");
            }
            else if (Math.Abs(posiciony) >= distanciaMov)
            {
                animation.SetTrigger("Bajar");
            }
        }

        if(Math.Abs(posiciony) <= distanciaMov && Math.Abs(posicionx) <= distanciaMov)
        {
            animation.SetTrigger("Parar");
        }
        
    }
    
}
