﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hablar : MonoBehaviour
{
    string[] instruccion;
    string[] fase;
    int frase;
    int count;
    int size;
    bool inicio;

    public TextToSpeech tts;
    public BallMove ball;

    // Start is called before the first frame update
    void Start()
    {
        instruccion = new string[4]
        {
            "Primer nivel",
            "Desliza tu dedo al lugar indicado según las instrucciones",
            "Al fallar el teléfono vibrara",
            "Mantén el dedo en la pantalla para oír las instrucciones nuevamente"

        };
        frase = 0;
        count = 0;
        tts = GetComponent<TextToSpeech>();
        tts.Speak(instruccion[0], "SuccesInstruccion");

        TTSInstruccionP();

    }

    

    public void TTSHablar(string texto)
    {
        tts.Speak(texto, "Succes");
    }

    public void TTSInstruccionP()
    {
            tts.Speak(instruccion[frase], "SuccesInstruccionP");    
    }

    public void TTSInstruccion()
    {
        frase++;
        tts.Speak(instruccion[frase], "SuccesInstruccion");
    }        


    public void TTSFases(string[] instruccion)
    {
        fase = instruccion;
        size = fase.Length;
        tts.Speak(fase[count], "SuccesFases");
    }


    /*Succes*/

    public void SuccesInstruccionP(string msg)
    {        
        switch (msg)
        {
            case "100":
                if (frase == 0)
                {
                }                
                break;
            case "200":
                frase++;                
                if (frase < 4)
                {
                    tts.Speak(instruccion[frase], "SuccesInstruccionP");
                }
                else
                {
                    frase = 0;
                }
                    
                break;
            case "300":
                tts.ShowMessage("Error");
                break;
        }
    }

    public void SuccesInstruccion(string msg)
    {
        switch (msg)
        {
            case "100":
                if (frase == 0)
                {
                }
                break;
            case "200":
                frase++;
                if (frase < 4)
                {
                    if (frase == 3)
                    {
                        tts.ShowMessage("Si entra");                        
                        tts.Speak(instruccion[frase], "SuccesRestart");
                        frase = 0;
                    }
                    else
                    {
                        tts.Speak(instruccion[frase], "SuccesInstruccion");
                    }
                }

                break;
            case "300":
                tts.ShowMessage("Error");
                break;
        }
    }

    public void SuccesFases(string msg)
    {
        switch (msg)
        {
            case "100":
                break;
            case "200":
                count++;
                if (count < size)
                {
                    tts.Speak(fase[count], "SuccesFases");
                }
                else
                {
                    count = 0;
                    ball.accion();
                }

                break;
            case "300":
                tts.ShowMessage("Error");
                break;
        }
    }

    public void Succes(string msg)
    {
        switch (msg)
        {
            case "100":
                break;
            case "200":
                break;
            case "300":
                break;
        }
    }
}
